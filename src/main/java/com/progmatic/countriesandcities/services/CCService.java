/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.countriesandcities.services;

import com.progmatic.countriesandcities.daos.CityAutoDao;
import com.progmatic.countriesandcities.daos.CountryAutoDao;
import com.progmatic.countriesandcities.dtos.CityDto;
import com.progmatic.countriesandcities.dtos.CountryDto;
import com.progmatic.countriesandcities.entities.City;
import com.progmatic.countriesandcities.entities.Country;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceContext;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author peti
 */
@Service
public class CCService {

    private static final Logger LOG = LoggerFactory.getLogger(CCService.class);

    @Autowired
    CountryAutoDao countryAutoDao;

    @Autowired
    CityAutoDao cityAutoDao;

    @Autowired
    private DozerBeanMapper beanMapper;

    @PersistenceContext
    EntityManager em;

    public List<CountryDto> listAllCountries() {
        //TODO get them from the db
        List<Country> allCountries = new ArrayList<>();
        List<CountryDto> allCountryDtos = em.createQuery("Select C From Country C").getResultList();
        allCountries.forEach((country) -> {
            allCountryDtos.add(beanMapper.map(country, CountryDto.class, "countryMapWithoutCities"));
        });
        return allCountryDtos;
    }

    public List<CityDto> listAllCities() {
        //TODO get them from the db
        List<City> allCities = new ArrayList<>();
        List<CityDto> allCountryDtos = em.createQuery("Select C From City C").getResultList();
        allCities.forEach((country) -> {
            allCountryDtos.add(beanMapper.map(country, CityDto.class));
        });
        return allCountryDtos;
    }

    @Transactional
    public CountryDto detailedCountryData(String id) {
        //TODO load c from the database
        //make somehow sure that all it's cities are also loaded
       
        Country c = (Country)em.createQuery("Select c from Country c where c.iso =:id")
                .setParameter("id", id).getSingleResult();
        c.getCities().size();
        if (c == null) {
            return null;
        }
        CountryDto cdto = beanMapper.map(c, CountryDto.class);
        return cdto;
    }

    //TODO: not implemented 
    //delete the country and make sure somehow that all of it's cities are also deleted.
    @Transactional
    public void deleteCountry(String id) {

        Country c = em.find(Country.class, id);

        em.remove(c);

    }

    @Transactional
    public void deleteCity(String id) {
        City c = em.find(City.class, id);
        Country country = c.getCountry();
        City capital = country.getCapital();
        if (capital != null && capital.getId().equals(id)) {
            LOG.info("Capital of {} is deleted.", country.getName());
            country.setCapital(null);
        }
        em.remove(c);
    }

    @Transactional
    public CityDto createCity(CityDto cdto) {
        em.persist(cdto);
        String id = UUID.randomUUID().toString();
        cdto.setId(id);
        listAllCities().add(cdto);
        return cdto;
    }
}

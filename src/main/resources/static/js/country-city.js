$(function(){

	$.get('/rest/countries', function(response){
		$.each(response, function(index, value){
			var $option = $('<option></option>').text(value.iso);
			$('#city-country').append($option);
		});
	}, "json");
        


	$('body').on('click', '#add-city-button', function(){
		/*
			TODO: valamiért ez az eseménykezelő le sem fut...
		*/
		var cityData = {
			/*
				TODO: összeszedni az űrlap adatait
			*/
		};
		$.ajax({
			url: '/rest/cities',
			method : 'POST',
			data : cityData,
			dataType : 'json',
			complete : function(){
				/*
					TODO: Be kellene csukni a popupot
				*/
			}
		});
	});
        $('table').on('click', '.delete-country', function(){
		var iso = $(this).closest('tr').data('iso');
		if(confirm("Are you sure yo want to delete?")){
			$.ajax({
				url : '/rest/countries/'+iso,
				type : 'DELETE',
				success : (response) =>{
					alert(response);
				}

			});
			$(this).closest('tr').remove();
		}
	});

	$('table').on('click', '.delete-city', function(){
		var id = $(this).closest('tr').data('id');
		if(confirm('Are you sure you want to delete?')){
			$.ajax({
				url : '/rest/cities/'+id,
				type : 'DELETE',
				success : (response) =>{

				}
			});
			$(this).closest('tr').remove();
		}
	});

	
});